lugaru (1.2-7) unstable; urgency=medium

  * Team Upload

  [ Patrice Duroux ]
  * Update watch file (Closes: #1089920)

 -- Alexandre Detiste <tchet@debian.org>  Sat, 08 Feb 2025 18:16:04 +0100

lugaru (1.2-6) unstable; urgency=medium

  * Add dependency on libglu1-mesa-dev (Closes: #1022322)
  * Update lintian-overrides
  * Bump Standards-Version to 4.6.1 (no change needed)
  * Update copyright info

 -- Vincent Prat <vivi@debian.org>  Thu, 27 Oct 2022 19:01:04 +0200

lugaru (1.2-5) unstable; urgency=medium

  * Update watch file
  * Use debhelper-compat (and bump to version 13)
  * Bump Standards-Version to 4.5.0
  * Add Rules-Requires-Root statement
  * Add lintian override for *-documentation-outside-usr-share-doc
  * Fix license inconsistency for appstream metainfo file
  * Remove transitional packages
  * Add Forwarded statement to patches

 -- Vincent Prat <vivi@debian.org>  Thu, 10 Sep 2020 21:48:30 +0200

lugaru (1.2-4) unstable; urgency=medium

  * Update debhelper compatibility version to 11
  * Update Vcs fields to Salsa
  * Update Standards-Version to 4.1.4
  * Fix typo in patch description
  * Add upstream metadata

 -- Vincent Prat <vinceprat@free.fr>  Sun, 13 May 2018 20:44:38 +0200

lugaru (1.2-3) unstable; urgency=medium

  * Multi-arch metadata for openlugaru-data
  * Update Standards-Version to 4.1.1
  * Set priority of transitional packages to optional

 -- Vincent Prat <vinceprat@free.fr>  Sat, 30 Sep 2017 10:58:25 +0200

lugaru (1.2-2) unstable; urgency=medium

  * Install appdata file in /usr/share/metainfo
  * Multi-arch metadata for lugaru-data added
  * Fix alc_cleanup error on exit (Closes: #874086)

 -- Vincent Prat <vinceprat@free.fr>  Tue, 05 Sep 2017 23:24:12 +0100

lugaru (1.2-1) unstable; urgency=medium

  * New upstream release
  * Add various upstream docs to package, including campaign docs in -data
    package
  * Add upstream appstream file
  * Switch from using launch wrapper to binary directly
  * Update Standards-Version to 4.0.0
    - Remove Debian menu entry
  * Rename openlugaru to lugaru
  * Update dependencies
  * Move from contrib/non-free to main
  * Update homepage and VCS links
  * Update copyright file according to new licensing
  * Drop patches:
    - change-homedir-to-use-openlugaru-name.patch, no longer needed due to
      rename-revert.
    - dont-install-data.patch, no longer needed due to source package merge.
    - fixed-bad-resolution-on-first-run, backport from upstream, included in
      current release.
  * Add Fix-mismatched-usage-length-build-fail-on-g.patch, fixes compile issue
    on g++.
  * Update debhelpher compatibility version to 10
  * Update watch file version to 4

 -- Vincent Prat <vinceprat@free.fr>  Sun, 25 Jun 2017 16:26:26 +0200

openlugaru-data (0~20110520.1+hge4354-4) unstable; urgency=medium

  * Team upload.
  * Set XS-Autobuild: yes to allow source only uploads in the future.
  * Do a binary upload now because the package will otherwise not be updated.

 -- Markus Koschany <apo@debian.org>  Tue, 19 Jan 2016 20:24:59 +0100

openlugaru-data (0~20110520.1+hge4354-3) unstable; urgency=medium

  * Team upload.
  * Vcs-Browser: Use cgit and https.
  * Update Homepage and Source address. The project has moved to bitbucket.org.
  * Declare compliance with Debian Policy 3.9.6.
  * Use compat level 9 and require debhelper >= 9.
  * Drop transitional dummy package lugaru-data.

 -- Markus Koschany <apo@debian.org>  Thu, 14 Jan 2016 21:52:48 +0100

openlugaru (0~20110520.1+hge4354+dfsg-5) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 3.9.6.
  * Vcs-Browser: Switch to cgit and https.
  * Drop transitional dummy package lugaru.
  * openlugaru.desktop: Add keywords and a comment in German.
  * Update Homepage and Source address because the project has moved to
    bitbucket.org.

 -- Markus Koschany <apo@debian.org>  Thu, 14 Jan 2016 21:26:40 +0100

openlugaru (0~20110520.1+hge4354+dfsg-4.1) unstable; urgency=medium

  * Non-maintainer upload
  * Remove libjpeg8-dev from Build-Depends (Closes: #765805)

 -- Ondřej Surý <ondrej@debian.org>  Sun, 19 Oct 2014 10:20:02 +0200

openlugaru-data (0~20110520.1+hge4354-2) unstable; urgency=low

  * Rename package to openlugaru-data, in order to distinguish from
    official Lugaru which is still distributed by Wolfire Games
  * Update Standards-Version to 3.9.4
    - Set correct DEP-5 Format: link
    - Add "-b debian_data" in Vcs-Git field
  * Change to "canonical" VCS-links, ("anonscm")
  * Arand -> Martin, same person, now with real name

 -- Martin Erik Werner <martinerikwerner@gmail.com>  Fri, 16 Aug 2013 00:53:23 +0200

openlugaru (0~20110520.1+hge4354+dfsg-4) unstable; urgency=low

  * Rename package to openlugaru, in order to distinguish from official Lugaru
    which is still distributed by Wolfire Games
    - add debian/patches/change-homedir-to-use-openlugaru-name.patch
  * Update Standards-Version to 3.9.4
    - Add "-b debian_source" in Vcs-Git field
  * Change to "canonical" VCS-links, ("anonscm")

 -- Martin Erik Werner <martinerikwerner@gmail.com>  Fri, 16 Aug 2013 00:54:13 +0200

lugaru (0~20110520.1+hge4354+dfsg-3) unstable; urgency=low

  * Remove unnecessary linux-libc-dev dependency
  * dh compat level 9 for hardening build flags
  * Remove libglew1.7-dev and only use libglew-dev build-dependency
    (Closes: 675775), FTBFS

 -- Martin Erik Werner <martinerikwerner@gmail.com>  Sun, 03 Jun 2012 12:06:05 +0200

lugaru (0~20110520.1+hge4354+dfsg-2) unstable; urgency=low

  * Rework launcher script to start windowed and with no mouse grabbing per
    default.
  * Update manual page:
    - Information regarding fullscreen and input grabbing,
    - document -h|-help|--help commandline
  * Change Arand -> Martin, same person, now with real name
  * Fix naming of 128x128 icon so it is used properly
  * Standards-Version 3.9.3
    - Update DEP-5 Format: link
  * Add unversioned library depend alternatives
    - libglu1-mesa-dev | libglu-dev
    - libglew1.5-dev | libglew-dev
    - libsdl1.2-dev | libsdl-dev
    - zlib1g-dev | libz-dev
  * Rename and refresh patches
    - Add DEP-3 tag to dont-install-data.patch
  * Build-Depends on libpng-dev, change from libpng12-dev (Closes: 662424)

 -- Martin Erik Werner <martinerikwerner@gmail.com>  Mon, 05 Mar 2012 08:26:50 +0100

lugaru (0~20110520.1+hge4354+dfsg-1) unstable; urgency=low

  * Initial release (Closes: 626156)
  * Source for tarball taken from https://code.google.com/p/lugaru/ at
    commit e435472cb42f
  * Source has been repacked into lugaru and lugaru-data packages due to some
    of the data being non-free
  * debian/patches/0001-fixed-bad-resolution-on-first-run: commit c699faa41aaf
    from p/lugaru-experimental fixing 0x0 resolution config writeout on start

 -- Arand Nash <ienorand@gmail.com>  Mon, 30 May 2011 19:15:31 +0100

lugaru-data (0~20110520.1+hge4354-1) unstable; urgency=low

  * Initial release
  * Source for tarball taken from https://code.google.com/p/lugaru/ at
    commit e435472cb42f
  * Source has been repacked into lugaru and lugaru-data packages due to some
    of the data being non-free

 -- Arand Nash <ienorand@gmail.com>  Tue, 21 Jun 2011 22:54:00 +0200

lugaru (0~20110520.1+hge4354+dfsg-1) unstable; urgency=low

  * Initial release (Closes: 626156)
  * Source for tarball taken from https://code.google.com/p/lugaru/ at
    commit e435472cb42f
  * Source has been repacked into lugaru and lugaru-data packages due to some
    of the data being non-free
  * debian/patches/0001-fixed-bad-resolution-on-first-run: commit c699faa41aaf
    from p/lugaru-experimental fixing 0x0 resolution config writeout on start

 -- Arand Nash <ienorand@gmail.com>  Mon, 30 May 2011 19:15:31 +0100
